<?php

/**
 * DataLayer
 */
class BEAI_TagManager_Block_Tm extends Mage_Core_Block_Template
{
    public function __construct()
    {
    }

    public function dataLayer()
    {
        return Mage::getModel('beai_tagmanager/tagBuilder')->JSON();
    }
}