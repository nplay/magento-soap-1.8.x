<?php

class BEAI_Vitrine_Model_Product_Api extends Mage_Catalog_Model_Product_Api
{
    public function removed($products, $storeView)
    {
        $ids = array_map(function($product){
            return (int)$product->entity_id;
        }, $products);

        $ecomIds = Mage::getModel('catalog/product')
            ->setStoreId($storeView)
            ->getCollection()
            ->addFieldToFilter('entity_id', array('in'=> $ids))
            ->getAllIds();

        $ecomIds = array_flip($ecomIds);
        $notPresentIds = array();

        foreach($ids as $id){
            if(array_key_exists($id, $ecomIds) == false)
                $notPresentIds[] = $id;
        }

        return array_map(function($id){
            return ['entity_id' => $id];
        }, $notPresentIds);
    }

    public function info($productId, $store = null, $attributes = null, $identifierType = null)
    {
        $data = parent::info($productId, $store, $attributes, $identifierType);
        $data['hasNew'] = $this->hasNew($productId, $store, $identifierType);
        $data['isSeable'] = $this->isSeable($productId, $store, $identifierType);

        return $data;
    }

    protected function hasNew($productId, $store, $identifierType)
    {
        $product = $this->_getProduct($productId, $store, $identifierType);
        
        $curDate = Mage::getModel('core/date')->date('Y-m-d');
        $curDateStamp = strtotime($curDate);

		$from = $product->getData('news_from_date');
        $to = $product->getData('news_to_date');
        
        if($from == null && $to == null)
            return false;
        if($from && $to == null)
            return strtotime($from) <= $curDateStamp;
		
		return $curDateStamp >= strtotime($from) && $curDateStamp <= strtotime($to);
    }

    protected function isSeable($productId, $store, $identifierType)
    {
        $product = $this->_getProduct($productId, $store, $identifierType);
        return $product->getTypeInstance(true)->isSalable($product);
    }
}