<?php

namespace Beai\Model\Datalayer;

class Product
{
    public $id = null;
    public $availability;
    public $condition;
    public $created_at;
    public $description;
    public $entityId;
    public $image_link;
    public $link;
    public $price;
    public $product_type;
    public $title;
}