<?php

namespace Beai;
use Beai\Model\Enums;

class SDK
{
   public $enums = null;

   public function __construct()
   {
      $this->enums = new Enums();
   }
}