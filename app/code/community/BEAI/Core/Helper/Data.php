<?php

class BEAI_Core_Helper_Data extends Mage_Core_Helper_Abstract
{
   protected static $_allKeys;

   public function SDK()
   {
      $base_path = Mage::getModuleDir('', 'BEAI_Core') . '/libraries/vendor/autoload.php';
      $loader = require_once($base_path);
   }

   public function getSDK()
   {
      $this->SDK();

      return new \Beai\SDK();
   }

   public function enabled()
   {
      return Mage::helper('core')->isModuleEnabled('BEAI_Core')
         && Mage::getStoreConfig('beai_core/beai_core_general/active') == 1;
   }

   public function allKeys()
   {
      if(!self::$_allKeys)
      {
         self::$_allKeys = [];

         $stores = Mage::app()->getStores();
   
         foreach($stores as $store)
         {
            $storeID = $store->getId();
            
            $keys = $this->keys($storeID);
   
            $objKey = new \StdClass;
            $objKey->keys = $keys;
            $objKey->storeID = $storeID;
   
            self::$_allKeys[] = $objKey;
         }
      }

      return self::$_allKeys;
   }

   public function keys($storeID = null)
   {
      $keys = Mage::getStoreConfig('beai_core/beai_core_general/public_key', $storeID);
      return explode('|', $keys);
   }

   public function urlLoader()
   {
      return Mage::getStoreConfig('beai_core/beai_core_variabels/url_loader');
   }
}